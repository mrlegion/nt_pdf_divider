﻿using System.Windows;
using PdfDividerApp.View;
using PdfDividerApp.ViewModel;
using Unity;

namespace PdfDividerApp
{
    public class Bootstrap
    {
        private readonly IUnityContainer container;

        public Bootstrap()
        {
            this.container = new UnityContainer();
        }

        public void Run()
        {
            Init();
        }

        private void Init()
        {
            this.container.RegisterSingleton<ShellView>();
            this.container.RegisterSingleton<ShellViewModel>();

            var shell = this.container.Resolve<ShellView>();
            shell.DataContext = this.container.Resolve<ShellViewModel>();
            Application.Current.MainWindow = shell;
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.MainWindow.Show();
        }
    }
}